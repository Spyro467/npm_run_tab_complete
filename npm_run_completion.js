#!/usr/bin/env node

var fs = require('fs');
//console.log('Startting');

//console.log('Current directory: ' + process.cwd());

var filename = process.cwd() + '/package.json';
//console.log(filename);
var exists = fs.existsSync(filename);

    if (exists){
//        console.log('Have package.json');
        var pack = require(filename);
     //   console.log(JSON.stringify(pack));
        var display = '';
        for(var key in pack.scripts){
            display += key + ' ';
       //     console.log('elm: ' + pack.scripts[key]);
        }
        console.log(display);
    }else{
   //     console.log('No package.json');
    }

process.on('uncaughtException', function(err) {
  console.log('Caught exception: ' + err);
});

process.on('exit', function(code) {
  // do *NOT* do this
  setTimeout(function() {
    //console.log('This will not run');
  }, 0);
  //console.log('About to exit with code:', code);
});
