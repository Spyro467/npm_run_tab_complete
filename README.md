# README #

Tab completion for npm run section of a node application package.json file

# Install #

* Clone the repo to your machine
* Execute the install.sh script
* Start a new terminal window

# How it works #

Typing "npm run" and hitting TABTAB will display all property names of the scripts section inside your package.json file.