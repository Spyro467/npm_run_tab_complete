 #!/usr/bin/bash

echo "Registering Script..."

function parse_package_json(){
    COMPREPLY=()
    local word="${COMP_WORDS[COMP_CWORD]}"
    local completions="$(~/npm_run_completion.js)"
    COMPREPLY=( $(compgen -W "$completions" -- "$word") )
}

