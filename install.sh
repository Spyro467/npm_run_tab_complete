 #!/usr/bin/bash

pwd=`pwd`

echo ". $pwd/npm-run-completion.bash" >> ~/.bashrc
echo "complete -F parse_package_json npm" >> ~/.bashrc

cp npm_run_completion.js ~/npm_run_completion.js

